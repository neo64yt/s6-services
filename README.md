# s6-services

Personal s6 services I made for certain programs. Since these services were made on an Artix system, you may need to tailor them according to your system's s6 implementation.

Currently, I had made services for these programs:
+ [ly](https://github.com/fairyglade/ly/)
+ [ananicy-cpp](https://gitlab.com/ananicy-cpp/ananicy-cpp/)
+ VMware
+ [Waydroid](https://waydro.id)
